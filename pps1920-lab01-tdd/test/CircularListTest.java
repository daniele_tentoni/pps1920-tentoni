import lab01.tdd.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * The test suite for testing the CircularList implementation
 */
public class CircularListTest {

    private CircularList circularList;

    @BeforeEach
    void beforeEach() {
        this.circularList = new CircularListImpl();
    }

    @Test
    void testInitialSize(){
        assertTrue(this.circularList.isEmpty());
    }

    @Test
    void testListNotEmpty() {
        fillListWithManyElements(this.circularList, 1);
        assertFalse(this.circularList.isEmpty());
    }

    @Test
    void testListHasManyElements() {
        fillListWithManyElements(this.circularList, 5);
        assertEquals(5, this.circularList.size());
    }

    @Test
    void testListNextElement() {
        fillListWithManyElements(this.circularList, 1);
        assertTrue(this.circularList.next().isPresent());
        assertTrue(this.circularList.next().isPresent());
        assertTrue(this.circularList.next().isPresent());
    }

    @Test
    void testNextWithManyElements() {
        fillListWithManyElements(this.circularList, 5);
        assertTrue(this.circularList.next().isPresent());
        assertTrue(this.circularList.next().isPresent());
        assertTrue(this.circularList.next().isPresent());
        assertTrue(this.circularList.next().isPresent());
        assertTrue(this.circularList.next().isPresent());
        assertTrue(this.circularList.next().isPresent());
        assertTrue(this.circularList.next().isPresent());
    }

    @Test
    void testListPreviousElement() {
        fillListWithManyElements(this.circularList, 1);
        assertTrue(this.circularList.previous().isPresent());
        assertTrue(this.circularList.previous().isPresent());
        assertTrue(this.circularList.previous().isPresent());
    }

    @Test
    void testListPreviousWithManyElements() {
        fillListWithManyElements(this.circularList, 5);
        assertTrue(this.circularList.previous().isPresent());
        assertTrue(this.circularList.previous().isPresent());
        assertTrue(this.circularList.previous().isPresent());
        assertTrue(this.circularList.previous().isPresent());
        assertTrue(this.circularList.previous().isPresent());
        assertTrue(this.circularList.previous().isPresent());
        assertTrue(this.circularList.previous().isPresent());
        assertTrue(this.circularList.previous().isPresent());
        assertTrue(this.circularList.previous().isPresent());
        assertTrue(this.circularList.previous().isPresent());
    }

    @Test
    void testResetListWithElements() {
        fillListWithManyElements(this.circularList, 2);
        this.circularList.reset();
        assertTrue(this.circularList.isEmpty());
    }

    @Test
    void testNextWithEvenStrategy() {
        fillListWithManyElements(this.circularList, 4);
        var element = this.circularList.next(new EvenSelectStrategy());
        assertTrue(element.isPresent());
        assertEquals(2, element.get());
    }

    @Test
    void testNextWithMultipleOfStrategy() {
        fillListWithManyElements(this.circularList, 4);
        var element = this.circularList.next(new MultipleOfStrategy(3));
        assertTrue(element.isPresent());
        assertEquals(3, element.get());
    }

    @Test
    void testNextWithEqualsStrategy() {
        fillListWithManyElements(this.circularList, 4);
        var element = this.circularList.next(new EqualsStrategy(3));
        assertTrue(element.isPresent());
        assertEquals(3, element.get());
    }

    private void fillListWithManyElements(CircularList list, int count) {
        for(int i = 0; i < count; i++)
            list.add(i);
    }
}
