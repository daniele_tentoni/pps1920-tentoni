package lab01.tdd;

public class MultipleOfStrategy implements SelectStrategy {

    private int divisor;

    public MultipleOfStrategy(int divisor) {
        this.divisor = divisor;
    }

    @Override
    public boolean apply(int element) {
        return element % this.divisor == 0;
    }
}
