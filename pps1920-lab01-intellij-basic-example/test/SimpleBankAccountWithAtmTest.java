import lab01.example.model.AccountHolder;
import lab01.example.model.BankAccount;
import lab01.example.model.BankAccountWithAtm;
import lab01.example.model.SimpleBankAccountWithAtm;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

public class SimpleBankAccountWithAtmTest {

    private AccountHolder holder;
    private BankAccountWithAtm account;

    @BeforeEach
    void BeforeEach(){
        this.holder = new AccountHolder("Daniele", "Tentoni", 2);
        this.account = new SimpleBankAccountWithAtm(0, this.holder, 1);
    }

    @Test
    void testInitialBalance() {
        assertEquals(0, account.getBalance());
    }

    @Test
    void testDeposit() {
        account.depositWithAtm(holder.getId(), 100);
        assertEquals(99, account.getBalance());
    }

    @Test
    void testWrongDeposit() {
        account.depositWithAtm(holder.getId(), 100);
        assertNotEquals(100, account.getBalance());
    }

    @Test
    void testWithdraw() {
        account.depositWithAtm(holder.getId(), 100);
        account.withdrawWithAtm(holder.getId(), 50);
        assertEquals(48, account.getBalance());
    }

    @Test
    void testWrongWithdraw() {
        account.depositWithAtm(holder.getId(), 100);
        account.withdrawWithAtm(holder.getId(), 50);
        assertNotEquals(50, account.getBalance());
    }
}
