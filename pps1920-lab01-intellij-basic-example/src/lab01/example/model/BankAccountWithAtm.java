package lab01.example.model;

public interface BankAccountWithAtm extends BankAccount {

    /**
     * Allows the deposit of an amount on the account, if the given userID corresponds to the register holder ID
     * of the bank account. This ID acts like an "identification token" .
     * The bank take a fee for this operation.
     * @param userID The id of the user that wants do the deposit.
     * @param amount The amount of the deposit.
     */
    void depositWithAtm(int userID, double amount);

    /**
     * Allows the withdraw of an amount from the account, if the given usrID corresponds to the register holder ID
     * of the bank account. This ID acts like an "identification token" .
     * The bank take a fee for this operation.
     * @param userId The id of the user that wants do the deposit.
     * @param amount The amount of the withdraw.
     */
    void withdrawWithAtm(int userId, double amount);
}
