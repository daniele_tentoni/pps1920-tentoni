package lab01.tdd;

public class EqualsStrategy implements SelectStrategy {

    private int equalizer;

    public EqualsStrategy(int equalizer) {
        this.equalizer = equalizer;
    }

    @Override
    public boolean apply(int element) {
        return element == equalizer;
    }
}
