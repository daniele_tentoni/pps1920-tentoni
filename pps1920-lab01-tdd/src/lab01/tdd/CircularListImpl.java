package lab01.tdd;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

public class CircularListImpl implements CircularList {
    private List<Integer> intList = new LinkedList<>();
    private int index;

    @Override
    public void add(int element) {
        this.intList.add(element);
    }

    @Override
    public int size() {
        return this.intList.size();
    }

    @Override
    public boolean isEmpty() {
        return this.intList.isEmpty();
    }

    @Override
    public Optional<Integer> next() {
        if(++index == this.intList.size())
            index = 0;
        return Optional.of(this.intList.get(index));

    }

    @Override
    public Optional<Integer> previous() {
        if(--index <= 0)
            index = this.intList.size() - 1;
        return Optional.of(this.intList.get(index));
    }

    @Override
    public void reset() {
        this.intList.clear();
        this.index = 0;
    }

    @Override
    public Optional<Integer> next(SelectStrategy strategy) {
        Optional<Integer> element;
        int startIndex = index;
        do {
            element = this.next();
            if(startIndex == index)
                return Optional.empty();
        } while (element.isPresent() && !strategy.apply(element.get()));
        return element;
    }
}
