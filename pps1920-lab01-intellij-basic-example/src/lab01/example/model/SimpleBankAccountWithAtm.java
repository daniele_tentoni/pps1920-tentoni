package lab01.example.model;

public class SimpleBankAccountWithAtm extends SimpleBankAccount implements BankAccountWithAtm {

    private final double fee;

    public SimpleBankAccountWithAtm(double initialBalance, AccountHolder holder, double fee) {
        super(holder, initialBalance);
        this.fee = fee;
    }

    @Override
    public void depositWithAtm(int userId, double amount) {
        // Deposit amount.
        this.deposit(userId, amount);

        // Pay the fee.
        super.balance -= this.fee;
    }

    @Override
    public void withdrawWithAtm(int userId, double amount) {
        // Check for available amount.
        if(super.checkUser(userId) && super.isWithdrawAllowed(amount + this.fee)) {
            // Withdraw amount and pay the fee.
            balance -= amount + this.fee;
        }
    }
}
